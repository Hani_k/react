import React from 'react';
import { Card, CardImg, CardImgOverlay, CardTitle, Breadcrumb, BreadcrumbItem } from 'reactstrap';
import {  Link} from 'react-router-dom';
import { LoadingSpinner } from './LoadingSpinnerComponent';
import { BASE_URL } from "../shared/baseUrl";

    function RenderMenuItem ({dish, onClick}) {
        return (
            <Card>
                <Link to={`/menu/${dish.id}`}>
                    <CardImg width="100%" src={BASE_URL+ dish.image} alt={dish.name} />
                    <CardImgOverlay>
                        <CardTitle>{dish.name}</CardTitle>
                    </CardImgOverlay>
                </Link>
            </Card>
        );
    }

    const Menu = (props) => {

        const menu = props.dishes.dishes.map((dish) => {
            return (
                <div className="col-12 col-md-5 m-1"  key={dish.id}>
                    <RenderMenuItem dish={dish} />
                </div>
            );
        });
        if (props.dishes.isLoading) 
            return(
                <LoadingSpinner />
            );
        else if (props.dishes.ErrMess)
                return (
                    <h4> {props.dishes.errmess} </h4>
                );
        else
            return (
                <div className="container"> 
                    <div className="row">
                        <Breadcrumb>
                            <BreadcrumbItem>
                                <Link to='/home'>Home</Link>
                            </BreadcrumbItem>
                            <BreadcrumbItem active>
                                Menu
                            </BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h4>Menu</h4>
                        </div>
                    </div>
                    <div className="row">
                        {menu}
                    </div>
                </div>
            );
    }















// class Menu extends Component{

    
    
//     // renderDish(dish){
//     //     if (dish != null){
//     //         return(
                
//     //         // <Card key={dish.id} >
//     //         //     <CardImg width="100%" src={dish.image} alt={dish.name}></CardImg>
//     //         //     <CardBody>
//     //         //         <CardTitle> {dish.name} </CardTitle>
//     //         //         <CardSubtitle> {dish.description} </CardSubtitle>
//     //         //     </CardBody>
//     //         // </Card>
//     //         );
//     //     }
//     //     else{
//     //         return(
//     //         <div></div>
//     //         );
//     //     }
//     // }
//     render(){
//         const menu = this.props.dishes.map((dish) =>{
//             return(
//                 <div key={dish.id} className="col-12 col-sm-5 m-1 p-4">
//                     <Card onClick={() => this.props.onClick(dish.id)}>
//                         <CardImg width="100%" src={dish.image} alt={dish.name}></CardImg>
//                         <CardImgOverlay>
//                             <CardTitle> {dish.name} </CardTitle>
//                         </CardImgOverlay>
//                     </Card>
//               </div>
//             );
//         });
//         return(
//                 <div className="container">
//                     <div className="row">
//                         {menu}
//                     </div>
//                 </div>
                
        
//         );

//     }
// }

export default Menu;