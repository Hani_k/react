import React, { Component } from 'react';
import {Card, CardBody, CardImg, CardTitle,  CardText, Breadcrumb, BreadcrumbItem, Modal, ModalHeader, ModalBody, Button, Col, Row,Label, ModalFooter} from 'reactstrap';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { LocalForm, Control, Errors } from 'react-redux-form';
import { LoadingSpinner } from './LoadingSpinnerComponent';
import { BASE_URL } from "../shared/baseUrl";
import { FadeTransform, Fade, Stagger } from 'react-animation-components';

function RenderDish({dish}){
    return (
        <div className="col-12 col-sm-5">
            <FadeTransform in transformProps={{exitTransform: 'scale(0.5) translateY(-50%)'}}>
                <Card>
                    <CardImg width="100%" src={BASE_URL+dish.image} alt={dish.name}></CardImg>
                    <CardBody>
                        <CardTitle>{dish.name}</CardTitle>
                        <CardText>{dish.description}</CardText>
                    </CardBody>
                </Card>
                </FadeTransform>
        </div>
    );
}

function RenderComments({comments, postComment, dishId}){
    if ({comments} !==0)
    return(
        <div className="col-12 col-md-5">
            <h4>comments</h4>
            <ul className="list-unstyled" >
                <Stagger in>
                    { comments.map((comment)=>{
                                return(
                                    <Fade key={comment.id}>
                                        <li >
                                        <p>{comment.comment}</p>
                                        <p> -- {comment.author} , {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))} </p>
                                        </li>
                                    </Fade>
                                );
                            }) }
                </Stagger>
            </ul>
            <div className="col-12 mb-3">
            <CommentForm postComment={postComment} dishId={dishId} />
            </div>
        </div>
    );
    else
    return(
        <div></div>
    );
}

function DishDetail (props){
    if (props.dishesLodaing)
        return(
            <LoadingSpinner />
        );
    
    else if (props.dishesErrMess)
            return(
                <h4> {props.dishesErrMess} </h4>
            );
    else if (props.dish != null)
        return (
            <div className="container">
                <div className="row">
                        <Breadcrumb>

                            <BreadcrumbItem><Link to="/menu">Menu</Link></BreadcrumbItem>
                            <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                        </Breadcrumb>
                        <div className="col-12">
                            <h3>{props.dish.name}</h3>
                            <hr />
                        </div> 
                    </div>
                <div className="row">
                    <RenderDish dish={props.dish}></RenderDish>
                    <RenderComments comments={props.comments} postComment={props.postComment} dishId={props.dish.id} />
                    
                </div>
            </div>
        );
    else
    return(
        <div></div>
    );
}

const required = (value)=> value && value.length;
const maxLength = (len) => (value)=>  !(value) || (value.length) <len;
const minLength = (len) => (value) => value && value.length >=len;

class CommentForm extends Component{
    constructor(props){
        super(props);
        this.state = {
            commentmodal:false,
        };
        this.commentModaltoggle = this.commentModaltoggle.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    commentModaltoggle() {
        this.setState({
            commentmodal:!this.state.commentmodal
        });
        
    }

    handleSubmit(vals){
        this.commentModaltoggle();
        this.props.postComment(this.props.dishId,vals.rating, vals.author, vals.comment);
    }

    render(){
          return(
            <div>
            <Button type="button" className="btn btn-outline-secondary" onClick={this.commentModaltoggle}>
                <span className="fa fa-pencil"> Submit comment</span>
            </Button>
            <Modal isOpen={this.state.commentmodal} toggle={this.commentModaltoggle}>
                <ModalHeader toggle={this.commentModaltoggle}>
                    Submit Comment
                </ModalHeader>
                <ModalBody>
                    <LocalForm onSubmit={(vals)=>this.handleSubmit(vals)} >
                        <Row className="form-group">
                            <Label for="rating" className="col-12 col-md-3">Rate</Label>
                            <Col md={8}>
                                <Control.select model=".rating" id='rating' name="rating" className="form-control" defaultValue='2' >
                                    <option value='1' >1</option>
                                    <option value='2'>2</option>
                                    <option value='3'>3</option>
                                    <option value='4'>4</option>
                                    <option value='5' >5</option>
                                </Control.select>
                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Label htmlFor="author" className="col-12" >Name</Label>
                            <Col>
                                <Control.text model=".author" id="author" name="author" className="form-control" 
                                 validators={{
                                     required, maxLength:maxLength(15), minLength:minLength(3)}}
                                  />
                                <Errors
                                    className="text-danger"
                                    model=".author"
                                    show="touched"
                                        messages={{
                                            required: 'This field is Required ',
                                            maxLength:"Maximum number of charaters is 15 " ,
                                            minLength:"Minimum number of charaters is 3"  
                                        }}
                                />
                            </Col>
                        </Row>
                        <Row className="form-group">
                            <Label htmlFor="commnet" className="col-12">Message</Label>
                            <Col>
                                <Control.textarea model=".comment" id="comment" name="comment"
                                rows="6" className="col-12"
                                />
                            </Col>
                        </Row>
                        <ModalFooter>
                     <Button  type="submit"  className="btn btn-primary">Submit comment</Button>
                     <Button className="btn btn-secondary" type="button" onClick={this.commentModaltoggle}>Cancel</Button>
                </ModalFooter>
                    </LocalForm>
                </ModalBody>
                
            </Modal>
            </div>
        );
    }
}







// class DishDetail extends Component{
    
//     renderDish(dish){
//         return(
             
//             <div className="col-12 col-md-5 m-0">
//                 <Card>
//                     <CardImg width="100%" src={dish.image} alt="detail.name"></CardImg>
//                     <CardBody> 
//                         <CardTitle> {dish.name} </CardTitle>
//                         <CardText> {dish.description} </CardText>
//                     </CardBody>
//                 </Card>
//             </div>
        
//         );
        
//     }

//     renderComments(comments){
//         if(comments !=null)
//         return(
//             <div className="col-12 col-md-5 ">
//                 <h4>Comments</h4>
//                 <ul className="list-unstyled">
//                     { comments.map((comment)=>{
//                         return(
//                             <li key={comment.id}>
//                             <p>{comment.comment}</p>
//                             <p> -- {comment.author} , {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))} </p>
//                             </li>
//                         );
//                     }) }
//                 </ul>
//             </div>
//         );
//         else
//         return(
//             <div></div>
//         );

//     }
       
          
      
    
//     render(){
//         if(this.props.dish != null)
//         return(
            
//             <div className="container">
//                 <div className="row">
//                     {this.renderDish(this.props.dish)}
//                     {this.renderComments(this.props.dish.comments)}
//                 </div>
//             </div>
//         );
//         else
//         return(
//             <div> </div>
//         );
//     }
// }

export default DishDetail;