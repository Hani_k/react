import React, { Component } from 'react';
import Menu from './Menucomponent';
import DishDetail from './DishdetailComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Home from './HomeComponent';
import About from './AboutComponent';
import Contact from'./ContactComponent';
import {Switch, Route, Redirect, withRouter} from 'react-router-dom';
import { connect } from 'react-redux';
import { postComment, fetchDishes, fetchComments, fetchPromotions, fetchLeaders, postFeedback, fetchFeedback} from '../redux/ActionCreators';
import { actions } from 'react-redux-form';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

const mapStateToProps = state =>{
  return {
    dishes:state.dishes,
    comments:state.comments,
    promotions:state.promotions,
    leaders:state.leaders,
    feedbacks:state.feedbacks
  }
}
const mapDispatchToProps = dispatch => ({
  
  postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment)),
  fetchDishes:()=>dispatch(fetchDishes()),
  resetFeedbckForm: ()=>{dispatch(actions.reset('feedback'))},
  postFeedback: (firstname, lastname, telnum, email, agree, contactTyp,message)=> dispatch(postFeedback(firstname, lastname, telnum, email, agree, contactTyp,message)),
  fetchComments:()=>dispatch(fetchComments()),
  fetchPromotions: ()=>dispatch(fetchPromotions()),
  fetchLeaders: ()=> dispatch(fetchLeaders()),
  fetchFeedbacks:()=> dispatch(fetchFeedback()),
  
});

class Main extends Component {


//   onDishSelect(dishId) {
//     this.setState({ selectedDish: dishId});
//   }
    
  componentDidMount(){
    this.props.fetchDishes();
    this.props.fetchComments();
    this.props.fetchPromotions();
    this.props.fetchLeaders();
    this.props.fetchFeedbacks();
  }  


  render() {
    const HomePage= ()=>{
        return (
        <Home dish={this.props.dishes.dishes.filter((dish)=> dish.featured===true)[0]}
              dishLoading = {this.props.dishes.isLoading}
              dishesErrMess = {this.props.dishes.errmess}
               promotion={this.props.promotions.promotions.filter((promo)=> promo.featured===true)[0]}
               promoLoading={this.props.promotions.isLoading}
               promoErrMess={this.props.promotions.errMess}
               leader={this.props.leaders.leaders.filter((leader)=>leader.featured)[0]}
               leadrLoading={this.props.leaders.isLoading}
               leaderFailed={this.props.leaders.errMess}  />
        );
    }
        const DishWithId=({match})=>{
            
        return(
            <DishDetail dish={this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishId,10))[0]} 
              dishesLodaing={this.props.dishes.isLoading}
              dishesErrMess={this.props.dishes.errmess}
              comments={this.props.comments.comments.filter((comment) => comment.dishId === parseInt(match.params.dishId,10))} 
              postComment={this.props.postComment}
              />
        );
      }
    return (
        <div>
        <Header />
        <TransitionGroup>
            <CSSTransition key={this.props.location.key} classNames="page" timeout={300}>
              <Switch location={this.props.location}>
                  <Route path='/home' component={HomePage} />
                  <Route exact path='/menu' component={() => <Menu dishes={this.props.dishes} />} />
                  <Route path='/menu/:dishId' component={DishWithId} />
                  <Route path ='/contactus' component={()=>< Contact resetFeedbckForm={this.props.resetFeedbckForm} postFeedback={this.props.postFeedback} feedbacks={this.props.feedbacks.feedbacks}  />}/>
                  <Route path='/aboutus' component={()=> <About leaders={this.props.leaders.leaders}
                  leadrLoading={this.props.leaders.isLoading} leaderFailed={this.props.leaders.errMess}/>} />
                  <Redirect to="/home" />
                </Switch>
              </CSSTransition>
            </TransitionGroup>
        <Footer />
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Main));