import React ,{Component} from 'react';
import {Nav,Navbar, NavbarBrand, NavItem, NavbarToggler, Jumbotron, Collapse, Button, Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input, Col, ModalFooter} from 'reactstrap';
import { NavLink } from 'react-router-dom';

class Header extends Component {

    constructor (props){
        super(props);
        this.state= {
            isNavOpen: false,
            isModalLoginOpen:false,
        }
        this.toggleNav = this.toggleNav.bind(this)
        this.toggleLogin = this.toggleLogin.bind(this);
        this.handelLoginSubmiting = this.handelLoginSubmiting.bind(this);
    }
    toggleNav(){
        this.setState({
            isNavOpen: !this.state.isNavOpen,
        })
    }

    toggleLogin(event){
        this.setState({
            isModalLoginOpen: !this.state.isModalLoginOpen,
        });
    }
    handelLoginSubmiting(event){
        this.toggleLogin();
        alert("username : "+this.username.value+" Password : "+this.password.value);
        event.preveentDefault();

    }

    render(){
        return(
            <React.Fragment>
                <Modal isOpen={this.state.isModalLoginOpen} toggle={this.toggleLogin} >
                    <ModalHeader toggle={this.toggleLogin}>
                        log in
                    </ModalHeader>
                    <ModalBody>
                        <Form onSubmit={this.handelLoginSubmiting}>
                            <FormGroup row>
                                <Label htmlFor="username" className="col-md-3">User name</Label>
                                <Col md={6}>
                                <Input type="text" id="username" name="username"
                                innerRef={(input)=>{this.username=input}} />
                                </Col>
                            </FormGroup>
                            <FormGroup row>
                                <Label htmlFor="password" className="col-md-3">Password</Label>
                                <Col md={6}>
                                <Input type="password" id="password" name="password"
                                innerRef={(input)=> {this.password=input}} />
                                </Col>
                            </FormGroup>
                            <ModalFooter>
                                <Button type="submit" color="primary">Log in</Button>
                                <Button type="button" onClick={this.toggleLogin}>Cancel</Button>
                            </ModalFooter>
                        </Form>
                    </ModalBody>
                </Modal>
                <Navbar dark expand="md">
                    <div className="container">
                        <NavbarToggler  onClick={this.toggleNav}  className="mr-2"/>
                        <NavbarBrand className="mr-auto" href="/" >
                            <img src='assets/images/logo.png' width="50" height="40" alt="Restrant con fiusion"></img>
                        </NavbarBrand>
                        <Collapse isOpen={this.state.isNavOpen}  navbar>
                        <Nav navbar>
                            <NavItem>
                                <NavLink className="nav-link"  to='/home'><span className="fa fa-home fa-lg"></span> Home</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/aboutus'><span className="fa fa-info fa-lg"></span> About Us</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link"  to='/menu'><span className="fa fa-list fa-lg"></span> Menu</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink className="nav-link" to='/contactus'><span className="fa fa-address-card fa-lg"></span> Contact Us</NavLink>
                            </NavItem>
                            </Nav>
                        </Collapse>
                        <Nav>
                            <Button type="button" onClick={this.toggleLogin} color="primary" outline>
                                <span className="fa fa-sign-in fa-lg">log on</span>
                            </Button>
                        </Nav>
                    </div>
                </Navbar>
                <Jumbotron>
                    <div className="container">
                        <div className="row row-container">
                            <div className="col-12 col-6-sm">
                                <h3>Con fiusion</h3>
                                <p>We take inspiration from the World's best cuisines, and create a unique fusion experience. Our lipsmacking creations will tickle your culinary senses!</p>
                            </div>
                        </div>
                    </div>
                </Jumbotron>
            </React.Fragment>
        );
    }
}
export default Header;