import React from 'react';
import { Card, CardImg, CardBody, CardTitle, CardText, CardSubtitle } from "reactstrap";
import { BASE_URL } from "../shared/baseUrl";

import { LoadingSpinner } from './LoadingSpinnerComponent';
import { FadeTransform } from 'react-animation-components';

function HomeCard({item, isLoading, errmess}){
    if (isLoading){
        return(
            <LoadingSpinner /> 
        );
    }
    else if(errmess){
        return(
            <h4>{errmess}</h4>
        );
    }
    else 
        return(
            <FadeTransform in transformProps={{exitTransform: 'scale(0.5) translateY(-50%)'}}>
                <Card>
                    <CardImg width='100%' src={BASE_URL+ item.image} alt={item.name}>
                        
                    </CardImg>
                    <CardBody>
                        <CardTitle>
                            {item.name}
                        </CardTitle>
                        {item.designation ? <CardSubtitle>{item.designation}</CardSubtitle> : null }
                        <CardText>
                            {item.description}
                        </CardText>
                    </CardBody>
                </Card>
            </FadeTransform>
    );
}

function Home(props){
    return(
        <div className="container">
            <div className="row">
                <div className="col-12 col-md-4">
                    <HomeCard item={props.dish} isLoading={props.dishLoading}  errmess={props.dishesErrMess} />
                </div>
                <div className="col-12 col-md-4">
                <HomeCard item={props.promotion} isLoading={props.promoLoading}  errmess={props.promoErrMess} />
                </div>
                <div className="col-12 col-md-4">
                <HomeCard item={props.leader} isLoading={props.leadrLoading}  errmess={props.leaderFailed}/>
                </div>
            </div>
        </div>
    );
}
export default Home;