import * as ActionType from './ActionTypes';

export const Feedbacks =  (state={feedbacks:[], errmess:null}, action)=>{
    switch(action.type){
        case ActionType.ADD_FEEDBACKS: 
            return {...state, feedbacks:action.payload, errmess:null};
        
        case ActionType.FEEDBACK_FAILED: 
            return {...state, feedbacks:[], errmess:action.payload};

        case ActionType.ADD_FEEDBACK:
            var feedback = action.payload; 
            return     {...state, feedbacks:state.feedbacks.concat(feedback)};
        default:
            return state;
    }
};