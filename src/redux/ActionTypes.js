/* Adding new coment in the submet comment card*/
export const ADD_COMMENT = 'ADD_COMMENT';
/*     For Fetching the dishes data      */
export const DISHES_LOADING = 'DISHES_LOADING';
export const DISHES_FAILED = 'DISHES_FAILED';
export const ADD_DISHES = 'ADD_DISHES';
/*     For Fetching the Comments data      */
export const COMMETS_FAILED = "COMMENTS_FAILD";
export const ADD_COMMENTS = "ADD_COMMENTS";
/* for fetching promotions data */ 
export const PROMOTIONS_LOADING = "PROMOTIONS_LOADING";
export const PROMOTIONS_FAILED = "PROMOTIONS_FAILED";
export const ADD_PROMOTIONS = "ADD_PROMOTIONS";
/* for fetching leaders data */
 export const LEADERS_LOADING = "LEADERS_LOADING";
 export const LEADERS_FAILD = "LEADERS_FAILD";
 export const ADD_LEADERS = "ADD_LEADERS";

 export const ADD_FEEDBACK = "ADD_FEEDBACK";
 export const FEEDBACK_FAILED = "FEEDBACK_FAILED";
 export const ADD_FEEDBACKS = "ADD_FEEDBACKS";

