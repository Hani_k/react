import  * as ActionType from './ActionTypes';
import { BASE_URL} from '../shared/baseUrl';

export const addFeedback = (newFeedback)=>({
    type:ActionType.ADD_FEEDBACK,
    payload:newFeedback
});

export const postFeedback = (firstname, lastname, telnum, email, agree, contactType,message) =>(dispatch)=>{
    const newFeedback = {
        firstname: firstname,
        lastname: lastname,
        telnum:  telnum,
        email: email,
        agree: agree,
        contactType: contactType,
        message: message,
    };
    newFeedback.date = new Date().toISOString();
    return fetch(BASE_URL+"feedback",{
        method:"POST",
        body:JSON.stringify(newFeedback),
        headers:{
            "Content-type":"application/json"
        },
        credentials:"same-origin"
    })
    .then(response=>{
        if (response.ok){
            return response;
        }
        else{
            var error = new Error ("Error "+response.status+" : "+response.statusText);
            error.response= response;
            throw error;
        }
    },
    error=>{
        var errmess = new Error (error.message);
        throw errmess;
    })
    .then(response=>response.json())
    .then(response=>dispatch(addFeedback(response)))
    .catch(error=>dispatch(feedbackFailed(error.message)));
};
export const fetchFeedback=()=>(dispatch)=>{
    return fetch(BASE_URL+"feedback")
            .then(response=>{
                if (response.ok){
                    return response;
                }
                else {
                    var error = new Error ("Error "+response.status+" : "+response.statusText);
                    error.response = response;
                    throw error;
                }
            },
            error=>{
                var errmess = new Error (error.message);
                throw errmess;
            })
            .then(response=>response.json())
            .then(response=>dispatch(addFeedbacks(response)))
            .catch(error=>dispatch(feedbackFailed(error.message)));
};
export const addFeedbacks = (feedback)=>({
    type:ActionType.ADD_FEEDBACKS,
    payload:feedback
});
export const feedbackFailed = (errmess)=>({
    type:ActionType.COMMETS_FAILED,
    payload:errmess
});


export const addComment = (comment)=>({
    type:ActionType.ADD_COMMENT,
    payload:comment
});


export const postComment = (dishId, rating, author, comment)=> (dispatch)=>{
    const newComment = {
        dishId:dishId,
        rating: rating,
        author: author,
        comment: comment
    };
    newComment.date = new Date().toISOString();
    return fetch(BASE_URL+"comments",{
        method:"POST",
        body:JSON.stringify(newComment),
        headers:{
            "Content-Type":"application/json"
        },
        credentials:"same-origin",
    })
    .then(response=>{
        if (response.ok){
            return response;
        }
        else {
            var error = new Error("Error "+ response.status+" : "+response.statusText);
            error.response = response;
            throw error;
        }
    },
    error=>{
        var errmess= new Error (error.message);
        throw errmess;
    })
    .then(response=>response.json())
    .then(response=>dispatch(addComment(response)))
    .catch(error=>dispatch(commentsFailed(error.message)));
};

export const fetchDishes = ()=>(dispatch)=>{
    dispatch(dishesLoading(true));
    
    return fetch(BASE_URL+"dishes")
            .then(response=>{
                if (response.ok)
                    return response;
                else{
                    var error =  new Error ("Error "+response.status+": "+response.statusText);
                    error.response= response;
                    throw error;
                }
            },
            error=>{
                var errmess =new Error (error.message);
                throw errmess;
            })
            .then(response=>response.json())
            .then(dishes=>dispatch(addDishes(dishes)))
            .catch(error=>{
                console.log("post message"+error.message);
            alert("Your message coudl not be submited\nError : "+error.message)
        });
};
export const dishesLoading = ()=>({
    type:ActionType.DISHES_LOADING,
});
export const dishesFailed = (errmess)=>({
    type: ActionType.DISHES_FAILED,
    payload: errmess,
});
export const addDishes = (dishes)=>({
    type: ActionType.ADD_DISHES,
    payload: dishes,
});

export const fetchComments=()=>(dispatch)=>{
    return fetch(BASE_URL+"comments")
                .then(response=>{
                            if (response.ok)
                                return response;
                            else{
                                var error =  new Error ("Error"+response.status+": "+response.statusText);
                                error.response= response;
                                throw error;
                            }
                        },
                        error=>{
                            var errmess =new Error (error.message);
                            throw errmess;
                        })
                .then(response=>response.json())
                .then(comments=>dispatch(addComments(comments)))
                .catch(error=>dispatch(commentsFailed(error.message)));
};
export const addComments= (comments)=>({
    type:ActionType.ADD_COMMENTS,
    payload:comments
});
export const commentsFailed = (errmess)=>({
    type:ActionType.COMMETS_FAILED,
    payload:errmess
});

export const fetchPromotions= ()=>(dispatch)=>{
    dispatch(promotionsLoading(true));
    return fetch(BASE_URL+"promotions")
    .then(response=>{
        if (response.ok)
            return response;
        else{
            var error =  new Error ("Error"+response.status+": "+response.statusText);
            error.response= response;
            throw error;
        }
    },
    error=>{
        var errmess =new Error (error.message);
        throw errmess;
    })
                .then(response=>response.json())
                .then(promotions=>dispatch(addPromotions(promotions)))
                .catch(error=>dispatch(promotionsFailed(error.message)));
};
export const promotionsLoading = ()=>({
    type:ActionType.PROMOTIONS_LOADING,
});
export const promotionsFailed = (errmess)=>({
    type:ActionType.PROMOTIONS_FAILED,
    payload:errmess
});
export const addPromotions=(promotions)=>({
    type: ActionType.ADD_PROMOTIONS,
    payload:promotions
});

export const fetchLeaders = ()=>(dispatch)=>{
    dispatch(leadersLodaing(true));
    return fetch(BASE_URL+"leaders")
    .then(response=>{
        if (response.ok)
            return response;
        else{
            var error =  new Error ("Error"+response.status+": "+response.statusText);
            error.response= response;
            throw error;
        }
    },
    error=>{
        var errmess =new Error (error.message);
        throw errmess;
    })
            .then(response=>response.json())
            .then(leaders=>dispatch(addLeaders(leaders)))
            .catch(error=>dispatch(leadersFailed(error.message)));
};
const leadersLodaing= ()=>({
    type:ActionType.LEADERS_LOADING,
});
const leadersFailed = (errmess)=>({
    type:ActionType.LEADERS_FAILD,
    payload:errmess
});
const addLeaders = (leaders)=>({
    type:ActionType.ADD_LEADERS,
    payload:leaders
});