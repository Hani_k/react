import * as ActionType from './ActionTypes';

export const Comments = (state={errMess:null, comments:[]}, action)=>{
    switch(action.type){
        case ActionType.ADD_COMMENTS:
            return {...state, errMess:null, comments:action.payload};
        case ActionType.COMMETS_FAILED:
            return{...state, errMess:action.payload, comments:[]};
        case ActionType.ADD_COMMENT:
            var comment= action.payload;
            
            console.log('Comment : ',comment);
            return {...state, comments:state.comments.concat(comment)};
        default:
            return state;
    }
};